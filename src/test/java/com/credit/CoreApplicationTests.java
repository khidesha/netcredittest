package com.credit;

import com.credit.domain.model.financialinfo.FinancialInfo;
import com.credit.domain.model.user.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoreApplication.class)
public class CoreApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testLoanCalculator() throws Exception {
		SysUser user = new SysUser();
		user.setBirthday(LocalDate.of(LocalDate.now().getYear() - 18, LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()).plusDays(1));

		FinancialInfo financialInfo = new FinancialInfo(user, BigDecimal.valueOf(1000), BigDecimal.valueOf(500));

		Assert.assertEquals(BigDecimal.ZERO, financialInfo.calculateLoanAmount());

		user.setBirthday(LocalDate.now().minusYears(20).minusMonths(2));

		Assert.assertEquals(BigDecimal.valueOf(700), financialInfo.calculateLoanAmount());

		financialInfo.setSalary(BigDecimal.valueOf(0));

		financialInfo.setNetLiabilities(BigDecimal.valueOf(500));

		Assert.assertEquals(BigDecimal.ZERO, financialInfo.calculateLoanAmount());

		financialInfo.setSalary(BigDecimal.valueOf(350));

		financialInfo.setNetLiabilities(BigDecimal.valueOf(500));

		Assert.assertEquals(BigDecimal.valueOf(50), financialInfo.calculateLoanAmount());
	}

}
