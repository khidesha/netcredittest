package com.credit.interfaces.utils;

import org.springframework.validation.Errors;

@SuppressWarnings("serial")
public class InvalidRestRequestException extends RuntimeException {
    private Errors errors;

    public InvalidRestRequestException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}