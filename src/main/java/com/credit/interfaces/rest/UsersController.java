package com.credit.interfaces.rest;

import com.credit.application.service.FinancialService;
import com.credit.application.service.UserService;
import com.credit.domain.model.financialinfo.FinancialInfo;
import com.credit.domain.model.user.SysUser;
import com.credit.interfaces.commands.RegisterUserCommand;
import com.credit.interfaces.dto.UserView;
import com.credit.interfaces.utils.InvalidRestRequestException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;

@RestController(value = "usersController")
@RequestMapping(value = {"/api/rest/v1/users"})
public class UsersController {

    @Qualifier("userService")
    @Autowired
    UserService userService;

    @Autowired
    FinancialService financialService;

    @Autowired
    private SystemWideSaltSource saltSource;

    @Autowired
    Md5PasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.POST, value = "/register",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.ALL_VALUE)
    public ResponseEntity<UserView> create(@RequestBody @Validated RegisterUserCommand registerUser,
                                           BindingResult bindingResult, HttpServletRequest request,
                                           UriComponentsBuilder builder) {

        if (bindingResult.hasErrors()) {
            throw new InvalidRestRequestException("invalid.request", bindingResult);
        }

        SysUser user = new SysUser(
                registerUser.getEmail(),
                passwordEncoder.encodePassword(registerUser.getPassword(), saltSource.getSystemWideSalt()),
                registerUser.getFullname(),
                registerUser.getBirthday(),
                registerUser.getMobileNumber(),
                registerUser.getPersonalNumber()
        );

        SysUser newUser = userService.createUser(user);

        FinancialInfo financialInfo = financialService.createFinancialInfo(newUser,
                registerUser.getSalary(),
                registerUser.getNetLiabilities());


        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/api/rest/v1/users/{id}")
                .buildAndExpand(Long.toString(newUser.getId()))
                .toUri());

        return new ResponseEntity<>(UserView.fromData(newUser, financialInfo), headers, HttpStatus.CREATED);

    }


    @RequestMapping(method = RequestMethod.GET, value = "/check/basic")
    public ResponseEntity<UserView> getCurrentUser(@RequestParam(value="auth", required = true) String authentication) {
        String decoded = new String(Base64.decodeBase64(authentication));
        String[] usernamePass = decoded.split(":");
        SysUser user = userService.getUserByUsernamePassUnsecure(usernamePass[0], usernamePass[1]);

        FinancialInfo financialInfo = financialService.getFinancialInfo(user);

        return new ResponseEntity<>(UserView.fromData(user, financialInfo), HttpStatus.OK);
    }
}
