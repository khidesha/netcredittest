package com.credit.interfaces.commands;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

public class RegisterUserCommand {

    @NotBlank(message = "{user.email.notnull}")
    @Email(message = "{user.email.format}")
    private String email;

    @Size(min = 6, max = 50)
    @NotBlank(message = "{user.password.notnull}")
    private String password;

    @NotBlank(message = "{user.fullname.notnull}")
    private String fullname;

    @NotNull(message = "{user.fullname.notnull}")
    private LocalDate birthday;

    @NotBlank(message = "{user.mobileNumber.notnull}")
    @Pattern(regexp = "(\\+995)[0-9]{9}", message = "{validation.cell.phone.format}")
    private String mobileNumber;

    @NotBlank(message = "{user.personalNumber.notnull}")
    @Pattern(regexp = "[0-9]{11}", message = "{validation.personal.number.format}")
    private String personalNumber;

    private BigDecimal salary;

    private BigDecimal netLiabilities;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getNetLiabilities() {
        return netLiabilities;
    }

    public void setNetLiabilities(BigDecimal netLiabilities) {
        this.netLiabilities = netLiabilities;
    }
}
