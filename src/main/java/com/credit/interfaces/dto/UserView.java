package com.credit.interfaces.dto;

import com.credit.domain.model.financialinfo.FinancialInfo;
import com.credit.domain.model.user.SysUser;

import java.math.BigDecimal;
import java.time.LocalDate;

public class UserView {

    private String email;

    private String fullname;

    private LocalDate birthday;

    private String mobileNumber;

    private String personalNumber;

    private BigDecimal salary;

    private BigDecimal netLiabilities;

    private BigDecimal calculatedLoanAmount;

    public UserView(String email, String fullname, LocalDate birthday, String mobileNumber, String personalNumber,
                    BigDecimal salary, BigDecimal netLiabilities, BigDecimal calculatedLoanAmount) {
        this.email = email;
        this.fullname = fullname;
        this.birthday = birthday;
        this.mobileNumber = mobileNumber;
        this.personalNumber = personalNumber;
        this.salary = salary;
        this.netLiabilities = netLiabilities;
        this.calculatedLoanAmount = calculatedLoanAmount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getNetLiabilities() {
        return netLiabilities;
    }

    public void setNetLiabilities(BigDecimal netLiabilities) {
        this.netLiabilities = netLiabilities;
    }

    public BigDecimal getCalculatedLoanAmount() {
        return calculatedLoanAmount;
    }

    public void setCalculatedLoanAmount(BigDecimal calculatedLoanAmount) {
        this.calculatedLoanAmount = calculatedLoanAmount;
    }

    public static UserView fromData(SysUser newUser, FinancialInfo financialInfo) {
        return new UserView(
                newUser.getEmail(),
                newUser.getFullname(),
                newUser.getBirthday(),
                newUser.getMobileNumber(),
                newUser.getPersonalNumber(),
                financialInfo.getSalary(),
                financialInfo.getNetLiabilities(),
                financialInfo.calculateLoanAmount()
        );
    }
}
