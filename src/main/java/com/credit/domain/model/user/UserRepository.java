package com.credit.domain.model.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<SysUser, Long> {

    SysUser findOne(Long pk);

    SysUser save(SysUser user);

    SysUser findByEmail(String email);

    SysUser findByEmailAndPasswordHash(String email, String passwordHash);
}
