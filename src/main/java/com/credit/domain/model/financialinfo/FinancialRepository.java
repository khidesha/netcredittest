package com.credit.domain.model.financialinfo;

import com.credit.domain.model.user.SysUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialRepository extends CrudRepository<FinancialInfo, Long> {

    FinancialInfo save(FinancialInfo financialInfo);

    FinancialInfo findByUser(SysUser user);

}
