package com.credit.domain.model.financialinfo;

import com.credit.application.utils.Constants;
import com.credit.domain.model.user.SysUser;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class FinancialInfo implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "fin_id")
    private Long id;

    @OneToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="user_id")
    private SysUser user;

    @Column(name = "salary")
    private BigDecimal salary;

    @Column(name = "liabilities")
    private BigDecimal netLiabilities;

    public FinancialInfo() {
    }

    public FinancialInfo(SysUser user, BigDecimal salary, BigDecimal netLiabilities) {
        this.user = user;
        this.salary = salary;
        this.netLiabilities = netLiabilities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getNetLiabilities() {
        return netLiabilities;
    }

    public void setNetLiabilities(BigDecimal netLiabilities) {
        this.netLiabilities = netLiabilities;
    }

    public BigDecimal netChange() {
        return this.salary.subtract(this.netLiabilities);
    }

    public BigDecimal calculateLoanAmount() {
        if (this.user == null) {
            throw new IllegalStateException("user.not.null");
        }

        if (this.user.userAge() >= Constants.BORROWER_MIN_AGE
                && this.user.userAge() <= Constants.BORROWER_MAX_AGE) {
            BigDecimal moneyToFund = new BigDecimal(this.user.userAge() * 10 );

            if (moneyToFund.add(netChange()).compareTo(BigDecimal.ZERO) <= 0) {
                return BigDecimal.ZERO;
            }

            return moneyToFund.add(netChange());
        }
        return BigDecimal.ZERO;
    }
}
