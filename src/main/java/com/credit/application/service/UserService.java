package com.credit.application.service;

import com.credit.domain.model.user.SysUser;

public interface UserService {

    SysUser createUser(SysUser user);

    SysUser getUserByUsernamePassUnsecure(String username, String password);
}
