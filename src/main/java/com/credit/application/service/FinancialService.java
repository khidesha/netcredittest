package com.credit.application.service;

import com.credit.domain.model.financialinfo.FinancialInfo;
import com.credit.domain.model.user.SysUser;

import java.math.BigDecimal;

public interface FinancialService {

    FinancialInfo createFinancialInfo(SysUser user, BigDecimal salary, BigDecimal liabilities);

    FinancialInfo getFinancialInfo(SysUser user);
}
