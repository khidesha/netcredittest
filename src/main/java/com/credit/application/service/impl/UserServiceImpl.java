package com.credit.application.service.impl;

import com.credit.application.service.UserService;
import com.credit.domain.model.user.SysUser;
import com.credit.domain.model.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private SystemWideSaltSource saltSource;

    @Autowired
    Md5PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public SysUser createUser(SysUser user) {

        SysUser checkUser = userRepository.findByEmail(user.getEmail());

        if (checkUser != null) {
            throw new IllegalStateException("email.already.used");
        }

        return userRepository.save(user);
    }

    @Override
    public SysUser getUserByUsernamePassUnsecure(String username, String password) {
        String encodedPass = passwordEncoder.encodePassword(password, saltSource);
        return userRepository.findByEmailAndPasswordHash(username, encodedPass);
    }

}
