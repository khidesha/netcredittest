package com.credit.application.service.impl;

import com.credit.application.service.FinancialService;
import com.credit.domain.model.financialinfo.FinancialInfo;
import com.credit.domain.model.financialinfo.FinancialRepository;
import com.credit.domain.model.user.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component("financialService")
public class FinancialServiceImpl implements FinancialService {

    private final FinancialRepository financialRepository;

    @Autowired
    public FinancialServiceImpl(FinancialRepository financialRepository) {
        this.financialRepository = financialRepository;
    }

    @Override
    public FinancialInfo createFinancialInfo(SysUser user, BigDecimal salary, BigDecimal liabilities) {
        FinancialInfo financialInfo = new FinancialInfo(
                user,
                salary,
                liabilities
        );

        return financialRepository.save(financialInfo);
    }

    @Override
    public FinancialInfo getFinancialInfo(SysUser user) {
        return financialRepository.findByUser(user);
    }

}
