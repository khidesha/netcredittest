angular.module('Netcredit').controller('loginCtrl', ['$scope', '$rootScope', '$log', '$routeParams', 'securitySvc', 'usersSvc', '$translate', 'toaster',
    function ($scope, $rootScope, $log, $routeParams, securitySvc, usersSvc, $translate, toaster) {

        $translate('pages.login.html_title').then(function (msg) {
            window.document.title = msg;
        });

        initUser();

        function initUser() {
            $scope.registerUser = {
                email:"",
                password:"",
                fullname:"",
                birthday:"",
                mobileNumber:"",
                personalNumber:"",
                salary:"",
                netLiabilities:""
            }
        }

        $scope.helper = {
            confirmPassword:"",
            mobileNumber:"",
            userBirthday:""
        };

        $scope.login = {
            email:"",
            password:""
        };

        $scope.toastPop = function (error) {
            toaster.pop('warning', error.code, error.message, null, 'trustedHtml');
        };

        $scope.register = function () {
            if ($scope.registerUser.password !== $scope.helper.confirmPassword) {
                $scope.toastPop({message:"register_form.passwords_not_matches"});
                return;
            }

            $scope.registerUser.birthday = [
                parseInt($scope.helper.userBirthday.substring(4)),
                parseInt($scope.helper.userBirthday.substring(2, 4)),
                parseInt($scope.helper.userBirthday.substring(0, 2))
            ];

            $scope.registerUser.mobileNumber = '+' + $scope.helper.mobileNumber;

            usersSvc.register($scope.registerUser).then(function (data) {
                $scope.login($scope.registerUser.email, $scope.registerUser.password);
                initUser();
            }, function (error) {
                $scope.toastPop(error)
            });
        };

        $scope.login = function (email, password) {
            securitySvc.authenticate(email, password).then(function (data) {
            }, function (error) {
                $scope.error = true;
            });
        };

        $scope.logout = function () {
            securitySvc.logout();
            $scope.isAuthenticated = securitySvc.isAuthenticated();

        };

        $scope.isAuthenticated = securitySvc.isAuthenticated();

    }]);