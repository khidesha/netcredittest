angular.module('Netcredit').controller('dashboardCtrl', ['$scope', '$rootScope', 'securitySvc', '$location', '$translate', 'toaster', '$filter',
    function ($scope, $rootScope, securitySvc, $location, $translate, toaster, $filter) {

        $translate('pages.home.html_title').then(function (msg) {
            window.document.title = msg;
        });

        $scope.currentUser = securitySvc.getUserDetails();

    }]);