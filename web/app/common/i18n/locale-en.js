var i18nEn = {
    header:{
        project_name : "Netcredit"
    },
    login_page: {
        login: "Login",
        register: "Register"
    },
    info_view:{
        email: "Email",
        fullname: "Fullname",
        birthday: "Birthday",
        mobileNumber: "Mobile Number",
        personalNumber: "Personal Number",
        salary: "Salary",
        netLiabilities: "Net Liabilities",
        calculatedLoanAmount : "You can borrow {{calculatedLoanAmount}} GEL"
    },
    register_form:{
        email: "Email",
        password: "Password",
        confirmPassword: "Confirm Password",
        fullname: "Fullname",
        birthday: "Birthday",
        mobileNumber: "Mobile Number",
        personalNumber: "Personal Number",
        salary: "Salary",
        netLiabilities: "Net Liabilities",
        register: "Register"
    },
    login_form:{
        email: "Email",
        password: "Password",
        login: "Login"
    }
};