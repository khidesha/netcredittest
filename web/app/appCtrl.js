angular.module('Netcredit')
    .controller('appCtrl', ['$scope', '$rootScope', '$http', '$timeout',  '$log', 'securitySvc', '$route',
        function ($scope, $rootScope, $http, $timeout, $log, securitySvc, $route) {

            $scope.logout = function() {
                securitySvc.logout();
                $scope.isAuthenticated = securitySvc.isAuthenticated();
            };
        }]);