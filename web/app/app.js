'use strict';

var REST_API_URLS = {
    'SPR': 'http://localhost:8080/api/rest/v1'
};

//'application', ['modules']
var app = angular.module('Netcredit', [
    'ngRoute',
    'ngResource',
    'ngSanitize',
    'ngAnimate',
    'ngCookies',
    'pascalprecht.translate',
    'toaster',
    'ui.bootstrap',
    'ui.utils',
    'ui.validate'
]);

// define a constant
app.constant('REST_API_URLS', REST_API_URLS);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', '$logProvider', '$translateProvider',
    function ($routeProvider, $locationProvider, $httpProvider, $logProvider, $translateProvider) {

        $logProvider.debugEnabled(true);

        $httpProvider.defaults.withCredentials = true;
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        $httpProvider.interceptors.push(['$rootScope', '$location', '$q', function ($rootScope, $location, $q) {
            return {

                'request': function (request) {
                    $rootScope.$broadcast('loading-started');
                    return request || $q.when(request);
                },

                'response': function (response) {
                    $rootScope.$broadcast('loading-complete');
                    return response || $q.when(response);
                },

                'responseError': function (rejection) {
                    $rootScope.$broadcast('loading-complete');
                    return $q.reject(rejection);
                }

            };
        }]);


        //html5 URI
        $locationProvider.html5Mode(true);
        // redirect to index
        $routeProvider.otherwise({redirectTo: '/login'});

        //Dynamic load /partials/ and /js/controls/ Base on url
        $routeProvider
            .when('/', {
                templateUrl: '/features/common/dashboard/dashboardDisplay.html'
            })
            .when('/login', {
                templateUrl: '/features/common/login/loginDisplay.html'
            })
            .when('/login/:any', {
                templateUrl: '/features/common/login/loginDisplay.html'
            });

        $translateProvider
            .useCookieStorage()
            .translations('en', i18nEn)
            .translations('ka', i18nKa)
            .preferredLanguage('en');
    }
]);


app.run(['$rootScope', '$http', '$location', '$anchorScroll', '$routeParams', 'securitySvc', '$log', '$translate', 'usersSvc', '$route',
    function ($rootScope, $http, $location, $anchorScroll, $routeParams, securitySvc, $log, $translate, usersSvc, $route) {

        $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
        });

        $rootScope.$on("$routeChangeStart", function (event, currentRoute, previousRoute) {
            if (securitySvc.isAuthenticated() && $location.path() === '/login') {
                securitySvc.logout();
            }

            if (!securitySvc.isAuthenticated() && $location.path() !== '/login') {
                $location.path('/login');
            }
        });

        $rootScope.changeLocale = function (lang) {
            $translate.use(lang);

            var userSettings = angular.copy(securitySvc.getUserDetails().userSettings);
            userSettings.language = lang.toUpperCase();
            usersSvc.saveSettings(userSettings).then(function (data) {
                localStorage.setItem("userDetails", angular.toJson(data));
            }, function (error) {
                $log.log(error);
            });

            $route.reload();
        };

        $rootScope.activeLocaleClass = function (lang) {
            if ($translate.use() == lang) {
                return 'active';
            }
            return '';
        };


        $rootScope.$on("event:logged-in", function (event, currentRoute, previousRoute) {
            initUsers();
        });

        var initUsers = function () {
            var currentUser = securitySvc.getUserDetails();

            if (currentUser) {
                $rootScope.currentUser = securitySvc.getUserDetails();
            }
        };

        initUsers();
    }]);

//Loading
app.directive("loadingIndicator", ['$rootScope', function ($rootScope) {
    return {
        restrict: "A",
        replace: true,
        template: "<div class='{{classname}}'><span>Loading...</span></div>",
        link: function (scope, element, attrs) {

            scope.classname = attrs.classname;

            $rootScope.$on("loading-started", function (e) {
                element.css({"display": ""});
            });

            $rootScope.$on("loading-complete", function (e) {
                element.css({"display": "none"});
            });

        }
    };
}]);


app.directive('resize', ['$window', function ($window) {
    return function (scope, element, attr) {

        var w = angular.element($window);
        scope.$watch(function () {
            return {
                'h': window.innerHeight
            };
        }, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;

            scope.resizeWithOffset = function (offsetH) {

                return {
                    'min-height': (newValue.h - offsetH) + 'px'
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
}]);


