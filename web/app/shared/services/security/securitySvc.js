 /*
 * Todo: See also https://github.com/witoldsz/angular-http-auth
 */
angular.module('Netcredit')
    .factory("securitySvc", ['$rootScope', '$http', '$q', '$injector', '$timeout', 'base64Svs', 'httpBuffer', 'REST_API_URLS',
        function ($rootScope, $http, $q, $injector, $timeout, base64Svs, httpBuffer, REST_API_URLS) {

            var apiPrefix = REST_API_URLS.SPR + '/users';

            function authenticate(username, password) {

                var authorizationParam = base64Svs.encode(username + ':' + password);
                var authorizationHeader = 'Basic ' + authorizationParam;
                var authCheckUrl = apiPrefix + '/check/basic';

                $http.get(authCheckUrl, {params: { 'auth': authorizationParam }, headers: {}})
                    .success(function (data, status, headers, config) {

                        // setup local session
                        $http.defaults.headers.common['Authorization'] = authorizationHeader;
                        localStorage.setItem("authHeader", authorizationHeader);
                        localStorage.setItem("userDetails", angular.toJson(data));

                        $timeout(function() {
                            $rootScope.$apply(function () {
                                $rootScope.userDetails = getUserDetails();
                            });
                        });

                        // retry old requests
                        loginConfirmed(data, function () {
                            config.headers['Authorization'] = authorizationHeader;
                            return config;
                        });
                        console.log('login success');
                    }).error(function (data, status, headers, config) {
                        $rootScope.$broadcast('event:auth-forbidden', status);
                        logout();
                        throw new Error('Authentication failed');
                    });
            }

            function getUserDetails() {
                if (!isAuthenticated()) {
                    return null;
                }
                return angular.fromJson(localStorage.getItem('userDetails'));
            }

            function logout() {
                console.log('logout');
                delete $http.defaults.headers.common['Authorization'];
                localStorage.removeItem('authHeader');
                localStorage.removeItem('userDetails');

                $rootScope.userDetails = null;
            }

            function isAuthenticated(){
                return localStorage.getItem('authHeader') !== null;
            }

            /**
             * Call this function to indicate that authentication was successfull and trigger a
             * retry of all deferred requests.
             * @param data an optional argument to pass on to $broadcast which may be useful for
             * example if you need to pass through details of the user that was logged in
             * @param configUpdater an optional transformation function that can modify the
             * requests that are retried after having logged in.  This can be used for example
             * to add an authentication token.  It must return the request.
             */
            function loginConfirmed(data, configUpdater) {
                var updater = configUpdater || function (config) {
                        return config;
                    };
                $rootScope.$broadcast('event:auth-loginConfirmed', data);
                //Todo: retry somehow uses old headers needs review
                //httpBuffer.retryAll(updater);
            }

            function loginCancelled(data, reason) {
                httpBuffer.rejectAll(reason);
                $rootScope.$broadcast('event:auth-loginCancelled', data);
            }

            return {
                authenticate: authenticate,
                isAuthenticated: isAuthenticated,
                getUserDetails: getUserDetails,
                logout: logout
            }

        }])

    .factory('base64Svs', function () {
        var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        return {
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                do {
                    enc1 = keyStr.indexOf(input.charAt(i++));
                    enc2 = keyStr.indexOf(input.charAt(i++));
                    enc3 = keyStr.indexOf(input.charAt(i++));
                    enc4 = keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };
    })


/**
 * Private module, a utility, required internally by 'http-auth-interceptor'.
 */
    .factory('httpBuffer', ['$injector', function ($injector) {
        /** Holds all the requests, so they can be re-requested in future. */
        var buffer = [];

        /** Service initialized later because of circular dependency problem. */
        var $http;

        function retryHttpRequest(config, deferred) {
            function successCallback(response) {
                deferred.resolve(response);
            }

            function errorCallback(response) {
                deferred.reject(response);
            }

            $http = $http || $injector.get('$http');
            $http(config).then(successCallback, errorCallback);
        }

        return {
            /**
             * Appends HTTP request configuration object with deferred response attached to buffer.
             */
            append: function (config, deferred) {
                buffer.push({
                    config: config,
                    deferred: deferred
                });
            },

            /**
             * Abandon or reject (if reason provided) all the buffered requests.
             */
            rejectAll: function (reason) {
                if (reason) {
                    for (var i = 0; i < buffer.length; ++i) {
                        buffer[i].deferred.reject(reason);
                    }
                }
                buffer = [];
            },

            /**
             * Retries all the buffered requests clears the buffer.
             */
            retryAll: function (updater) {
                for (var i = 0; i < buffer.length; ++i) {
                    retryHttpRequest(updater(buffer[i].config), buffer[i].deferred);
                }
                buffer = [];
            },

            getLastDisplayUrl: function () {
                var API_URL = $injector.get('REST_API_URLS').SPR;
                for (var i = buffer.length; i-- > 0;) {
                    // ensure this was not API call
                    if (buffer[i].config.url.lastIndexOf(API_URL) != -1) {
                        continue;
                    }
                    return buffer[i].config.url;
                }
                return null;
            }
        };
    }])


    .config(['$routeProvider', '$locationProvider', '$httpProvider',
        function ($routeProvider, $locationProvider, $httpProvider) {

            if(localStorage.getItem('authHeader')){
                $httpProvider.defaults.headers.common['Authorization'] = localStorage.getItem('authHeader');
            }

            $httpProvider.interceptors.push(function ($rootScope, $location, $q, httpBuffer) {
                return {
                    'responseError': function (rejection) {
                        if (!rejection.config.ignoreAuthModule) {
                            switch (rejection.status) {
                                case 401:
                                    var deferred = $q.defer();
                                    httpBuffer.append(rejection.config, deferred);
                                    $rootScope.$broadcast('event:auth-loginRequired', rejection);
                                    return deferred.promise;
                                    break;
                                case 403:
                                    $rootScope.$broadcast('event:auth-forbidden', rejection);
                                    break;
                            }
                        }
                        // otherwise, default behaviour
                        return $q.reject(rejection);
                    }
                };
            });
        }])


    .run(['$rootScope', '$http', '$location', 'securitySvc', 'httpBuffer', '$log',
        function ($rootScope, $http, $location, securitySvc, httpBuffer, $log) {

            $rootScope.$on('event:auth-loginRequired', function (event) {
                if ($location.path() !== '/login' || $location.path() !== '/signup') {
                    $location.path('/login');
                }
            });

            $rootScope.$on('event:auth-loginConfirmed', function (event) {
                var lastDisplay = httpBuffer.getLastDisplayUrl();
                if (lastDisplay) {
                    //TODO: needs better impl
                    $location.path(lastDisplay.substring(0, lastDisplay.lastIndexOf($location.protocol() + '://' + $location.host)));
                } else {
                    $location.path('/');
                }
            });

        }]);
