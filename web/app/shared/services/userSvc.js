angular.module('Netcredit').factory("usersSvc", ['$http', '$q', '$log', 'REST_API_URLS', function($http, $q, $log,  REST_API_URLS) {

    var apiPrefix = REST_API_URLS.SPR + '/users';

    return {
        register : function (user) {
            var deferred = $q.defer();

            $http.post(apiPrefix + '/register', user)
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function(data, status, headers, config) {
                    if (status === 0) {
                        data = {};
                        data.message = 'No connection.';
                    }
                    deferred.reject(data, status);
                    $log.error('Error', status);
                });
            return deferred.promise;
        }
    };

}]);