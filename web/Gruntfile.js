// Generated on 2013-07-11 using generator-webapp 0.2.2
'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var modRewrite = require('connect-modrewrite');

var mountFolder = function (connect, dir) {
    /*return connect.static(require('path').resolve(dir));*/
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // configurable paths
    var yeomanConfig = {
        app: 'app',
        dist: 'dist'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        watch: {
            options: {
                nospawn: true
            },
            less: {
                files: ['<%= yeoman.app %>/static/styles/**/*.less'],
                tasks: ['less']
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                    '<%= yeoman.app %>/*.html',
                    '{.tmp,<%= yeoman.app %>}/static/css/{,*/}*.css',
                    '{.tmp,<%= yeoman.app %>}/static/styles/**/*.css',
                    '<%= yeoman.app %>/static/styles/.tmp/styles/*.css',
                    '{.tmp,<%= yeoman.app %>}/js/{,*/}*.js',
                    '<%= yeoman.app %>/static/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        connect: {
            options: {
                port: 9000,
                // change this to '0.0.0.0' to access the server from outside
                hostname: '0.0.0.0'
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            modRewrite([
                                '!/api|/static|/shared|\\.html|\\.js|\\.png|\\.jpg|\\.ico|\\.jpeg|\\.css|\\.woff|\\.ttf|\\.swf|\\.wav|\\.json|\\.docx$ /index.html'
                            ]),
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app),
                            lrSnippet
                        ];
                    }
                }
            },
            livereload2: {
                options: {
                    middleware: function (connect) {
                        return [
                            modRewrite([
                                '!/api|/static|/shared|\\.html|\\.js|\\.png|\\.jpg|\\.ico|\\.jpeg|\\.css|\\.woff|\\.ttf|\\.swf|\\.wav|\\.json|\\.docx$ /index2.html'
                            ]),
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app),
                            lrSnippet
                        ];
                    }
                }
            },
            test: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, 'test')
                        ];
                    }
                }
            },
            dist: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                path: 'http://localhost:<%= connect.options.port %>'
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },

        less: {
            development: {
                files: {
                    "<%= yeoman.app %>/static/styles/.tmp/styles/main.css": "<%= yeoman.app %>/static/styles/**/*.less"
                }
            }
        },

        "string-replace": {
            inline: {
                files: {
                    'app/index2.html': 'app/index.html'
                },
                options: {
                    replacements: [{
                        pattern: '<!--#include file="env.txt" -->',
                        replacement: "<%= grunt.file.read('app/env.txt') %>"
                    }]
                }
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/**/*.js',
                        '<%= yeoman.dist %>/**/**/*.css',
                        '<%= yeoman.dist %>/fonts/*',
                        '<%= yeoman.dist %>/sounds/*'
                    ]
                }
            }
        },
        useminPrepare: {
            options: {
                dest: '<%= yeoman.dist %>'
            },
            html: '<%= yeoman.app %>/index.html'
        },
        usemin: {
            options: {
                dirs: ['<%= yeoman.dist %>']
            },
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
            css: ['<%= yeoman.dist %>/static/css/{,*/}*.css']
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/static/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },
        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/static/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },
        htmlmin: {
            dist: {
                options: {
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>',
                    src: '*.html',
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },
        uglify: {
            options: {
                mangle: false
            }
        },
        // Put files not handled in other tasks here
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,txt}',
                        '.htaccess',
                        'static/images/**/*.{webp,gif,jpeg,jpg,svg,png}',
                        'common/**',
                        'features/**/*.html',
                        'shared/**/*.html'
                    ]
                }, {
                    expand: true,
                    flatten: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>/fonts',
                    src: [
                        'static/fonts/*'
                    ]
                }, {
                    expand: true,
                    flatten: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>/static/sounds',
                    src: [
                        'static/sounds/*'
                    ]
                } ,{
                    expand: true,
                    flatten: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>/static/fonts',
                    src: [
                        'vendor/bootstrap/dist/fonts/*'
                    ]
                } , {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: [
                        'generated/*'
                    ]
                }]
            }
        },
        concurrent: {
            server: [
                'less'
            ],
            test: [
                'less'
            ],
            dist: [
                'less',
                'imagemin',
                'svgmin',
                'htmlmin'
            ]
        }
    });

    grunt.registerTask('server', function () {
        var target = grunt.option('target');
        if (target === 'dist') {
            return grunt.task.run(['build', 'open', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('remote-server', function () {
        var target = grunt.option('target');
        if (target === 'dist') {
            return grunt.task.run(['build', 'open', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'connect:livereload2',
            'string-replace',
            'watch'
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'useminPrepare',
        'concurrent:dist',
        'concat',
        'cssmin',
        'uglify',
        'copy',
        'rev',
        'usemin'
    ]);

    grunt.registerTask('default', [
        'test',
        'build'
    ]);
};